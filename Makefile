CC= gcc
OPTION1= -Wall -ansi `pkg-config --cflags MLV` `pkg-config --libs-only-other --libs-only-L MLV`
OPTION2= `pkg-config --libs-only-l MLV`

main : initialisation.o affichage_terminal.o
	$(CC) $(OPTION1) initialisation.o affichage_terminal.o $(OPTION2) -o main

affichage_terminal.o : affichage_terminal.c
	$(CC) $(OPTION1) -c affichage_terminal.c

initialisation.o : initialisation.c initialisation.h 
	$(CC) $(OPTION1) -c initialisation.c 

clean:
	rm -f *.o *~ main
