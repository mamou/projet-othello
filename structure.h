#ifndef structure_h
#define structure_h

typedef int plateau[8][8];/* le plateau est un tableau de taille 8 par 8 */

typedef struct{
  char j1,j2,null;
}joueur; /* joueur vaudra soit j1 soit j2 soit rien si la partie est finie */

typedef struct{
  plateau p;
  joueur j;
}jeu;/*le jeu est définit par le plateau sur lequel on joue, et le joueur à qui c'est le tour */

typedef struct{
  int x;
  int y;
}coup; /* un coup est représenté par deux coordonnées x et y. */

#endif
